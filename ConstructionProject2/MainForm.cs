﻿using ConstructionProject2.Classes;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace ConstructionProject2
{
    public partial class MainForm : Form
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        ConstructionWarehouse warehouse = new ConstructionWarehouse();

        public MainForm()
        {
            InitializeComponent();
            ClearInput();
        }

        private void ClearInput()
        {
            nameTextBox.Text = string.Empty;
            characteristicTextBox.Text = string.Empty;
            unitTextBox.Text = string.Empty;
            priceTextBox.Text = string.Empty;
            materialCodeNumeric.Value = 1;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            TSMI_English_Click(sender, e);
            TSDD_Language.Text = "English (United States)";

            ClearInput();
            startTimer();

            //ClassSerialize.DeserializationFromXml<List<Material>>(ref warehouse.MaterialsList, "Information.xml");
            ClassSerialize.DeserializationFromBinary<List<Material>>(ref warehouse.MaterialsList, "Information.bin");

            for (int i = 0; i < warehouse.MaterialsList.Count; i++)
                dgv.Rows.Add(warehouse.MaterialsList[i].name, warehouse.MaterialsList[i].characteristic,
                    warehouse.MaterialsList[i].unit, warehouse.MaterialsList[i].price, warehouse.MaterialsList[i].materialCode);

            logger.Debug("Старт работы программы.");
        }

        private void startTimer()
        {
            AppConfiguration myConfig = new AppConfiguration();
            ClassSerialize.DeserializationFromXml<AppConfiguration>(ref myConfig, "Settings.xml");

            myTimer.Interval = myConfig.timeInterval;
            myTimer.Enabled = true;
            myTimer.Tick += myTimer_Tick;
            myTimer.Start();

            logger.Debug("Произошел запуск таймера.");
            logger.Info("Таймер запущен.");
        }

        void myTimer_Tick(object sender, EventArgs e)
        {
            logger.Debug("Попытка выполнения автосериализации(по времени таймера).");
            if (ClassSerialize.SerializeToXml<List<Material>>(ref warehouse.MaterialsList, "Information.xml"))
            {
                logger.Info("Выполнена автосериализации(по времени таймера) в XML.");
            }

            if (ClassSerialize.SerializeToJson<List<Material>>(ref warehouse.MaterialsList, "Information.json"))
            {
                logger.Info("Выполнена автосериализации(по времени таймера) в JSON.");
            }
        }

        
        private void showMessage(bool methodResult)
        {
            if (methodResult)
            {
                labelStatus.Text = "Дані успішно збережено.";
            }
            else
            {
                labelStatus.Text = "Вибачте, сталася помилка.";
            }
        }

        private void doSerialize(string wayOfSerialization)
        {
            if(wayOfSerialization == "xml")
            {
                showMessage(ClassSerialize.SerializeToXml<List<Material>>(ref warehouse.MaterialsList, "Information.xml"));

                logger.Debug("Попытка выполнения сериализации в XML.");
                logger.Info("Выполнена сериализации в XML.");
            }
            else if(wayOfSerialization == "json")
            {
                showMessage(ClassSerialize.SerializeToJson<List<Material>>(ref warehouse.MaterialsList, "Information.json"));

                logger.Debug("Попытка выполнения сериализации в JSON.");
                logger.Info("Выполнена сериализации в JSON.");
            }
            else if(wayOfSerialization == "binary")
            {
                showMessage(ClassSerialize.SerializeToBinary<List<Material>>(ref warehouse.MaterialsList, "Information.bin"));

                logger.Debug("Попытка выполнения сериализации в BIN.");
                logger.Info("Выполнена сериализации в JSON.");
            }
            else
            {
                MessageBox.Show(
                    "Вибачте, сталася помилка.",
                    "Помилка!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                logger.Error("Произошла ошибка. Сериализация не выполнена.");
            }

        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            AppConfiguration myConfig = new AppConfiguration();
            ClassSerialize.DeserializationFromXml<AppConfiguration>(ref myConfig, "Settings.xml");

            string format = myConfig.defaultWayForSerialization;

            if (rdbXml.Checked)
            {
                format = "xml";
            }
            else if(rdbJson.Checked)
            {
                format = "json";
            }
            else if(rdbBinary.Checked)
            {
                format = "binary";
            }

            logger.Debug("Запрос на выполнение сериализации.");
            doSerialize(format);
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            ClearInput();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            Material myMaterial = new Material(
                nameTextBox.Text, characteristicTextBox.Text, unitTextBox.Text,
                Convert.ToDouble(priceTextBox.Text.ToString()), Convert.ToInt32(materialCodeNumeric.Value));

            warehouse.MaterialsList.Add(myMaterial);
            dgv.Rows.Add(myMaterial.name, myMaterial.characteristic, myMaterial.unit, myMaterial.price, myMaterial.materialCode);

            labelStatus.Text = "Запис успішно додано.";

            logger.Debug("Произошло сохранение новой записи.");
            logger.Info("Новая запись добавлена в List.");
            ClearInput();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            //ClassSerialize.SerializeToXml<List<Material>>(ref warehouse.MaterialsList, "Information.xml");
            //ClassSerialize.SerializeToJson<List<Material>>(ref warehouse.MaterialsList, "Information.json");
            //ClassSerialize.SerializeToBinary<List<Material>>(ref warehouse.MaterialsList, "Information.bin");

            AppConfiguration myConfig = new AppConfiguration();
            ClassSerialize.DeserializationFromXml<AppConfiguration>(ref myConfig, "Settings.xml");

            string format = myConfig.defaultWayForSerialization;

            doSerialize(format);

            Application.Exit();

            logger.Info("Завершение работы программы.");
        }

        Point LastPoint;

        private void MainForm_MouseDown(object sender, MouseEventArgs e)
        {
            LastPoint = new Point(e.X, e.Y);
        }

        private void MainForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - LastPoint.X;
                this.Top += e.Y - LastPoint.Y;
            }
        }

        private void ChangeLanguage(string newLanguageString)
        {
            var resources = new ComponentResourceManager(typeof(MainForm));
            CultureInfo newCultureInfo = new CultureInfo(newLanguageString);
            foreach (Control c in this.Controls)
            { 
                resources.ApplyResources(c, c.Name, newCultureInfo); 
            }
            resources.ApplyResources(this, "$this", newCultureInfo);
            foreach (var item in SS_Status.Items.Cast<ToolStripItem>().Where(item => (item is ToolStripStatusLabel) != false)) {
                resources.ApplyResources(item, item.Name, newCultureInfo);
            }

            TSDD_Language.Text = newCultureInfo.NativeName;
            SetCurrenLanguageButtonChecked();
        }

        private void SetCurrenLanguageButtonChecked()
        {
            foreach (ToolStripMenuItem languageButton in TSDD_Language.DropDownItems)
            {
                languageButton.Checked = (languageButton.Text == TSDD_Language.Text);
            }
        }

        private void TSMI_Russian_Click(object sender, EventArgs e)
        {
            ChangeLanguage("ru-RU");
        }

        private void TSMI_English_Click(object sender, EventArgs e)
        {
            ChangeLanguage("en-EN");
        }

        private void TSMI_Ukranian_Click(object sender, EventArgs e)
        {
            ChangeLanguage("uk-UK");
        }
    }
}
