﻿using ConstructionProject2.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ConstructionProject2
{
    [Serializable]
    public class ConstructionWarehouse
    {
        public List<Material> MaterialsList = new List<Material>();

        public ConstructionWarehouse() { }
    }
}
