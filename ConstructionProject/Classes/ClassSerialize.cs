﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace ConstructionProject2
{
    public static class ClassSerialize
    {
        public static bool SerializeToXml<T>(ref T inObject, string inFileName)
        {
            try
            {
                System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(T));
                using (FileStream fs = new FileStream(inFileName, FileMode.OpenOrCreate))
                {
                    writer.Serialize(fs, inObject);
                }

                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(
                    ex.Message,
                    "Помилка!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return false; 
            }
        }

        public static void DeserializationFromXml<T>(ref T inObject, string inFileName)
        {
            if(System.IO.File.Exists(inFileName))
            {
                System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(T));
                System.IO.StreamReader file = new System.IO.StreamReader(inFileName);
                inObject = (T)reader.Deserialize(file);
                file.Close();

            }
            else
            {
                MessageBox.Show(
                    "Файл не існує!",
                    "Помилка!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        public static bool SerializeToJson<T>(ref T inObject, string inFileName)
        {

            try
            {
                DataContractJsonSerializer writer = new DataContractJsonSerializer(typeof(T));
                using (FileStream fs = new FileStream(inFileName, FileMode.OpenOrCreate))
                {
                    writer.WriteObject(fs, inObject);
                }

                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(
                    ex.Message,
                    "Помилка!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return false; 
            }
        }
    }
}
