﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ConstructionProject2.Classes
{
    [Serializable]
    public class Material
    {
        [XmlElement] public string name = "";
        [XmlElement] public string characteristic = "";
        [XmlElement] public string unit = "";
        [XmlElement] public double price = 0;
        [XmlElement] public int materialCode = 0;

        public Material() { }

        public Material(string name, string characteristic, string unit, double price, int materialCode)
        {
            this.name = name;
            this.characteristic = characteristic;
            this.unit = unit;
            this.price = price;
            this.materialCode = materialCode;
        }
    }
}
