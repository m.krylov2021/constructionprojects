﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ConstructionProject2.Classes
{
    [Serializable]
    public class AppConfiguration
    {
        [XmlElement]public int timeInterval = 1;

        public AppConfiguration() { }
    }
}
