﻿using ConstructionProject2.Classes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ConstructionProject2
{
    public partial class MainForm : Form
    {
        ConstructionWarehouse warehouse = new ConstructionWarehouse();

        public MainForm()
        {
            InitializeComponent();
            ClearInput();
        }

        private void ClearInput()
        {
            nameTextBox.Text = string.Empty;
            characteristicTextBox.Text = string.Empty;
            unitTextBox.Text = string.Empty;
            priceTextBox.Text = string.Empty;
            materialCodeNumeric.Value = 1;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ClearInput();
            startTimer();

            ClassSerialize.DeserializationFromXml<List<Material>>(ref warehouse.MaterialsList, "Information.xml");

            for(int i = 0; i < warehouse.MaterialsList.Count; i++)
                dgv.Rows.Add(warehouse.MaterialsList[i].name, warehouse.MaterialsList[i].characteristic,
                    warehouse.MaterialsList[i].unit, warehouse.MaterialsList[i].price, warehouse.MaterialsList[i].materialCode);
        }

        private void startTimer()
        {
            AppConfiguration myConfig = new AppConfiguration();
            ClassSerialize.DeserializationFromXml<AppConfiguration>(ref myConfig, "Settings.xml");

            myTimer.Interval = myConfig.timeInterval;
            myTimer.Enabled = true;
            myTimer.Tick += myTimer_Tick;
            myTimer.Start();
        }

        void myTimer_Tick(object sender, EventArgs e)
        {
            ClassSerialize.SerializeToXml<List<Material>>(ref warehouse.MaterialsList, "Information.xml");
            ClassSerialize.SerializeToJson<List<Material>>(ref warehouse.MaterialsList, "Information.json");
        }

        private void saveButtonToXml_Click(object sender, EventArgs e)
        {
            if (ClassSerialize.SerializeToJson<List<Material>>(ref warehouse.MaterialsList, "Information.json"))
            {
                MessageBox.Show(
                    "Дані успішно збережено.",
                    "Повідомлення",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(
                    "Вибачте, сталася помилка.",
                    "Помилка!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        private void saveButtonToJson_Click(object sender, EventArgs e)
        {
            if (ClassSerialize.SerializeToJson<List<Material>>(ref warehouse.MaterialsList, "Information.json"))
            {
                MessageBox.Show(
                    "Дані успішно збережено.",
                    "Повідомлення",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(
                    "Вибачте, сталася помилка.",
                    "Помилка!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            ClearInput();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            Material myMaterial = new Material(
                nameTextBox.Text, characteristicTextBox.Text, unitTextBox.Text,
                Convert.ToDouble(priceTextBox.Text.ToString()), Convert.ToInt32(materialCodeNumeric.Value));

            warehouse.MaterialsList.Add(myMaterial);
            dgv.Rows.Add(myMaterial.name, myMaterial.characteristic, myMaterial.unit, myMaterial.price, myMaterial.materialCode);

            MessageBox.Show(
                    "Запис успішно додано.",
                    "Повідомлення!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

            ClearInput();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            ClassSerialize.SerializeToXml<List<Material>>(ref warehouse.MaterialsList, "Information.xml");
            ClassSerialize.SerializeToJson<List<Material>>(ref warehouse.MaterialsList, "Information.json");

            Application.Exit();
        }

        Point LastPoint;

        private void MainForm_MouseDown(object sender, MouseEventArgs e)
        {
            LastPoint = new Point(e.X, e.Y);
        }

        private void MainForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - LastPoint.X;
                this.Top += e.Y - LastPoint.Y;
            }
        }
    }
}
