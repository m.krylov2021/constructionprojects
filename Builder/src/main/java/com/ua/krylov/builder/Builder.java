package com.ua.krylov.builder;

import com.ua.krylov.entity.car.CarType;
import com.ua.krylov.entity.car.Engine;
import com.ua.krylov.entity.car.GPSNavigator;
import com.ua.krylov.entity.car.Transmission;
import com.ua.krylov.entity.car.TripComputer;

public interface Builder {
    void setCarType(CarType type);
    void setSeats(int seats);
    void setEngine(Engine engine);
    void setTransmission(Transmission transmission);
    void setTripComputer(TripComputer tripComputer);
    void setGPSNavigator(GPSNavigator gpsNavigator);
}