package com.ua.krylov;

import com.ua.krylov.builder.CarBuilder;
import com.ua.krylov.builder.CarManualBuilder;
import com.ua.krylov.director.Director;
import com.ua.krylov.entity.car.Car;
import com.ua.krylov.entity.manual.Manual;

public class Main {
    public static void main(String[] args) {
        Director director = new Director();

        CarBuilder builder = new CarBuilder();
        director.constructCityCar(builder);

        Car car = builder.getResult();
        System.out.println("Car built:\n" + car.getCarType());

        CarManualBuilder manualBuilder = new CarManualBuilder();

        director.constructSportsCar(manualBuilder);
        Manual carManual = manualBuilder.getResult();
        System.out.println("\nCar manual built:\n" + carManual.print());
    }
}
