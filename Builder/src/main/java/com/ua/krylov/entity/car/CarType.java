package com.ua.krylov.entity.car;

public enum CarType {
    CITY_CAR, SPORTS_CAR, SUV
}